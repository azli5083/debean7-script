#!/bin/bash

# go to root
cd

# disable ipv6
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
sed -i '$ i\echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6' /etc/rc.local

#repository
echo "deb http://kebo.vlsm.org/debian/ jessie main contrib non-free" >> /etc/apt/sources.list 
echo "deb http://kebo.vlsm.org/debian/ jessie-updates main contrib non-free" >> /etc/apt/sources.list
echo "deb http://kebo.vlsm.org/debian-security/ jessie/updates main contrib non-free" >> /etc/apt/sources.list
apt-get update
apt-get -y upgrade
#pritunl 
sudo tee -a /etc/apt/sources.list.d/mongodb-org-3.6.list << EOF
deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.6 main
EOF

sudo tee -a /etc/apt/sources.list.d/pritunl.list << EOF
deb http://repo.pritunl.com/stable/apt jessie main
EOF

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A
sudo apt-get update
sudo apt-get --assume-yes install pritunl mongodb-org
sudo systemctl start mongod pritunl
sudo systemctl enable mongod pritunl
cd
pritunl set app.server_port 4430
pritunl set app.redirect_server false

#webmin 
apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python -y
echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
echo "deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib" >> /etc/apt/sources.list
wget http://rricketts.com/wp-content/uploads/2015/02/jcameron-key.asc
apt-key add jcameron-key.asc
apt-get update
apt-get install webmin -y
service webmin restart

# initialisasi var
export DEBIAN_FRONTEND=noninteractive
OS=`uname -m`;
MYIP=$(wget -qO- ipv4.icanhazip.com);
MYIP2="s/xxxxxxxxx/$MYIP/g";

# install squid3
apt-get -y install squid3
wget -O /etc/squid3/squid.conf "https://gitlab.com/azli5083/debean7-script/raw/master/squid3.conf"
sed -i $MYIP2 /etc/squid3/squid.conf;
service squid3 restart

# install dropbear
apt-get -y install dropbear
sed -i 's/NO_START=1/NO_START=0/g' /etc/default/dropbear

sed -i 's/DROPBEAR_PORT=22/DROPBEAR_PORT=443/g' /etc/default/dropbear

sed -i 's/DROPBEAR_EXTRA_ARGS=/DROPBEAR_EXTRA_ARGS="-p 109 -p 110"/g' /etc/default/dropbear

echo "/bin/false" >> /etc/shells

echo "/usr/sbin/nologin" >> /etc/shells
	q\
#sed -i 's/DROPBEAR_BANNER=""/DROPBEAR_BANNER="bannerssh"/g' /etc/default/dropbear
service ssh restart

service dropbear restart

cd

#fail2ban
apt-get -y install fail2ban 
service fail2ban restart

#stunnel4
cd
wget https://github.com/azli5083/pritunl.ubuntu14/raw/master/stunnel4 && bash stunnel4 && rm stunnel4

# install neofetch
echo "deb http://dl.bintray.com/dawidd6/neofetch jessie main" | sudo tee -a /etc/apt/sources.list
curl -L "https://bintray.com/user/downloadSubjectPublicKey?username=bintray" -o Release-neofetch.key && sudo apt-key add Release-neofetch.key && rm Release-neofetch.key
apt-get update
apt-get install neofetch

# install ddos deflate
apt-get -y install dnsutils dsniff
wget https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/ddos-deflate-master.zip
unzip ddos-deflate-master.zip
cd ddos-deflate-master
./install.sh

# download script
cd /usr/bin
wget -O menu "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/menu.sh"
wget -O usernew "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/usernew.sh"
wget -O trial "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/trial.sh"
wget -O hapus "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/hapus.sh"
wget -O cek "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/user-login.sh"
wget -O userlimit "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/userlimit.sh"
wget -O userlimitssh "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/userlimitssh.sh"
wget -O member "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/user-list.sh"
wget -O resvis "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/resvis.sh"
wget -O speedtest "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/speedtest_cli.py"
wget -O info "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/info.sh"
wget -O about "https://gitlab.com/azli5083/debean7-script/raw/e717318351cf60fe06de2c91f28da2acd0754056/about.sh"

chmod +x menu
chmod +x usernew
chmod +x trial
chmod +x hapus
chmod +x cek
chmod +x member
chmod +x resvis
chmod +x speedtest
chmod +x info
chmod +x userlimit
chmod +x userlimitssh
chmod +x about

# finishing
service openvpn restart
service ssh restart
service dropbear restart
service squid3 restart
service fail2ban restart
service webmin restart
rm -rf ~/.bash_history && history -c
echo "unset HISTFILE" >> /etc/profile

# info
clear
echo "Autoscript Include:" | tee log-install.txt
echo "===========================================" | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "Service"  | tee -a log-install.txt
echo "-------"  | tee -a log-install.txt
echo "OpenSSH  : 22, 143"  | tee -a log-install.txt
echo "Dropbear : 80, 443"  | tee -a log-install.txt
echo "Squid3   : 8080, 3128 (limit to IP SSH)"  | tee -a log-install.txt
echo "PritunlWeb : http://$MYIP:4430"  | tee -a log-install.txt
echo "badvpn   : badvpn-udpgw port 7300"  | tee -a log-install.txt
echo "stunnel4 : ssl port 442"  | tee -a log-install.txt
echo "sila taip menu         (Menampilkan daftar perintah yang tersedia)"  | tee -a log-install.txt
cd
rm -f /root/deb8.sh
echo "Sila copy key dibawah dan paste di Pritunl anda : http://$MYIP:4430"
pritunl setup-key